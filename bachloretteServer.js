const express = require('express');
const cors = require('cors');
const app = express()

require('dotenv').config()

app.use(cors());

const DatabaseHandler = require('./DatabaseHandler.js');
const db = new DatabaseHandler();

app.get('/', function (req, res) {
  db.getCandidates().then((cans) => {
    res.send(JSON.stringify(cans));
  })
});

app.put('/candidate/:id', function (req, res) {
  const id = req.params.id;
  if (id === undefined) res.sendStatus(404);

  db.getCandidate(id).then((c) => {
    res.send(JSON.stringify(c));
  });
});

app.patch('/candidate/:id/enabled/:enabled', function (req, res) {
  const id = req.params.id;
  const enabled = req.params.enabled;
  if (id === undefined || enabled === undefined) res.sendStatus(404);
  db.setCandidateEnabledStatus(id, enabled == 'true').then((can) => {
    res.send(JSON.stringify(can));
  });
});

app.patch('/candidateLike/:id/', function (req, res) {
  const id = req.params.id;
  if (id === undefined) res.sendStatus(404);

  db.addLikeToCandidate(id).then((can) => {
    res.send(JSON.stringify(can));
  });
});
 
app.listen(process.env.SERVER_PORT);
console.log(`Server listening on ${process.env.SERVER_PORT}`)