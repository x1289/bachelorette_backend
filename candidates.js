// TODO: add all faces of https://www.rtl.de/cms/sendungen/show/die-bachelorette/kandidaten.html
const Candidates = [
  {
    Id: 0,
    Name: 'Benedikt Pfisterer',
    Geburtstag: '22.06.1990',
    Wohnort: 'Karlsruhe',
    Größe: '1,83m',
    Beruf: 'Key Account Manager',
    SingleSeit: 'eineinhalb Jahren',
    Likes: 0,
    active: true
  },
  {
    Id: 1,
    Name: 'Dario Carlucci',
    Geburtstag: '10.07.1987',
    Wohnort: 'München',
    Größe: '1,85m',
    Beruf: 'Key Account Manager & Model',
    SingleSeit: 'eineinhalb Jahren',
    Likes: 0,
    active: true
  },
  {
    Id: 2,
    Name: 'Dominik Bobinger',
    Geburtstag: '21.07.1990',
    Wohnort: 'Donauwörth',
    Größe: '1,88m',
    Beruf: 'Projektleiter in der Baubranche',
    SingleSeit: 'zwei Jahren',
    Likes: 0,
    active: true
  },
  {
    Id: 3,
    Name: 'Gustav Masurek',
    Geburtstag: '25.04.1989',
    Wohnort: 'Paderborn',
    Größe: '1,86m',
    Beruf: 'Betriebsleiter in der Gastronomie',
    SingleSeit: 'ca. einem Jahr',
    Likes: 0,
    active: true
  },
  {
    Id: 4,
    Name: 'Hendrik Luczak',
    Geburtstag: '25.08.1987',
    Wohnort: 'Hannover',
    Größe: '2,05m',
    Beruf: 'Key Account Manager',
    SingleSeit: 'Oktober 2020',
    Likes: 0,
    active: true
  },
  {
    Id: 5,
    Name: 'Jonathan Steinig',
    Geburtstag: '04.10.1995',
    Wohnort: 'Flensburg',
    Größe: '1,88m',
    Beruf: 'Model',
    SingleSeit: '/',
    Likes: 0,
    active: true
  },
  {
    Id: 6,
    Name: 'Julian Dannemann',
    Geburtstag: '10.08.1993',
    Wohnort: 'Hamburg',
    Größe: '1,93m',
    Beruf: 'Trader',
    SingleSeit: 'einem Jahr',
    Likes: 0,
    active: true
  },
  {
    Id: 7,
    Name: 'Kenan Engerini',
    Geburtstag: '29.11.1985',
    Wohnort: 'Wien, Österreich',
    Größe: '1,81m',
    Beruf: 'Chemieingenieur',
    SingleSeit: 'einem Jahr',
    Likes: 0,
    active: true
  },
  {
    Id: 8,
    Name: 'Kevin Ullmann',
    Geburtstag: '20.05.1994',
    Wohnort: 'Hannover',
    Größe: '1,90m',
    Beruf: 'Unternehmer',
    SingleSeit: 'einem Jahr',
    Likes: 0,
    active: true
  },
  {
    Id: 9,
    Name: 'Lars Maucher',
    Geburtstag: '05.10.1996',
    Wohnort: 'Zainingen',
    Größe: '1,85m',
    Beruf: 'Industriemechaniker und Fitnesstrainer',
    SingleSeit: 'eineinhalb Jahren',
    Likes: 0,
    active: true
  },
  {
    Id: 10,
    Name: 'Leon Knudsen',
    Geburtstag: '15.09.1987',
    Wohnort: 'Hamburg',
    Größe: '1,86m',
    Beruf: 'Business Controller',
    SingleSeit: 'drei Jahren',
    Likes: 0,
    active: true
  },
  {
    Id: 11,
    Name: 'Maurice Deufel',
    Geburtstag: '03.05.1995',
    Wohnort: 'Radolfzell',
    Größe: '1,85m',
    Beruf: 'Student',
    SingleSeit: 'vier Jahren',
    Likes: 0,
    active: true
  },
  {
    Id: 12,
    Name: 'Max Adrio',
    Geburtstag: '11.08.1989',
    Wohnort: 'Stuttgart',
    Größe: '1,83m',
    Beruf: 'Investmentberater',
    SingleSeit: 'zwei Jahren',
    Likes: 0,
    active: true
  },
  {
    Id: 13,
    Name: 'Maximilian Witt',
    Geburtstag: '10.07.1995',
    Wohnort: 'Köln',
    Größe: '1,80m',
    Beruf: 'Vertriebler im Außendienst',
    SingleSeit: 'sechs Monaten',
    Likes: 0,
    active: true
  },
  {
    Id: 14,
    Name: 'Nico Cenk Bilic',
    Geburtstag: '02.05.1991',
    Wohnort: 'Berlin',
    Größe: '1,86m',
    Beruf: 'Mietwagenunternehmer',
    SingleSeit: 'einem Jahr',
    Likes: 0,
    active: true
  },
  {
    Id: 15,
    Name: 'Niko Ulanovsky',
    Geburtstag: '06.01.1997',
    Wohnort: 'Oberstdorf',
    Größe: '1,78m',
    Beruf: 'Eiskunstläufer und -trainer',
    SingleSeit: 'eineinhalb Jahren',
    Likes: 0,
    active: true
  },
  {
    Id: 16,
    Name: 'Raphael Fasching',
    Geburtstag: '26.02.1998',
    Wohnort: 'Leopoldsdorf, Österreich',
    Größe: '1,83m',
    Beruf: 'Fotograf',
    SingleSeit: 'zwei Jahren',
    Likes: 0,
    active: true
  },
  {
    Id: 17,
    Name: 'Robert Janjetov',
    Geburtstag: '24.02.1998',
    Wohnort: 'Berlin',
    Größe: '1,83m',
    Beruf: 'Industriemechatroniker',
    SingleSeit: 'sechs Monaten',
    Likes: 0,
    active: true
  },
  {
    Id: 18,
    Name: 'Tony Lübke',
    Geburtstag: '27.07.1990',
    Wohnort: 'Rostock',
    Größe: '1,79m',
    Beruf: 'Polizeibeamter',
    SingleSeit: 'eineinhalb Jahren',
    Likes: 0,
    active: true
  },
  {
    Id: 19,
    Name: 'Zico Banach',
    Geburtstag: '24.02.1991',
    Wohnort: 'Köln',
    Größe: '1,93m',
    Beruf: 'Development Manager',
    SingleSeit: 'zweieinhalb Jahren',
    Likes: 0,
    active: true
  },
];

module.exports = Candidates;