const fs = require('fs-extra')
const candidates = require('./candidates');

const DB_FILE = './db.json';

class DatabaseHandler {
  constructor() {
    fs.ensureFileSync(DB_FILE);
    fs.readJSON(DB_FILE, (err, db) => {
      if (err) console.error(err);
      if (db === undefined) {
        fs.writeJSONSync(DB_FILE, {candidates});
      } else {
      }
    })
    this.initState().then((c) => {
      this.state = c;
    });
  }

  initState() {
    return new Promise((resolve, reject) => {
      let state = {};
      fs.readJSON(DB_FILE, (err, db) => {
        if (err) console.error(err);
        if (db !== undefined) {
          state = db;
        } else {
          state = {candidates};
        }
        const functions = {
          getCandidates: () => {
            return {candidates: state.candidates};
          },
          getCandidate: (id) => {
            return state.candidates[id];
          },
          addLikeToCandidate: (id) => {
            if (state.candidates[id] !== undefined) {
              state.candidates[id].Likes += 1;
            }
            return state.candidates[id];
          },
          setCandidateEnabledStatus(id, enabled) {
            if (state.candidates[id] !== undefined) {
              state.candidates[id].active = enabled === true;
            }
            return state.candidates[id];
          }
        }
        resolve(functions);
      });
    })
  }

  getCandidates() {
    return new Promise((resolve, reject) => {
      const cans = this.state.getCandidates();
      if (cans !== undefined) {
        resolve(cans);
      }
      fs.readJSON(DB_FILE, (err, db) => {
        if (err) {
          console.error(err)
          resolve({candidates: []});
        }
        if (db !== undefined && db.candidates !== undefined) {
          resolve({candidates: db.candidates});
        } else {
          resolve({candidates: []});
        }
      })
    }, (reason) => {
      resolve({candidates: []});
    });
  }

  getCandidate(id) {
    return new Promise((resolve, reject) => {
      const can = this.state.getCandidate(id);
      if (can !== undefined) {
        resolve(can);
      }
      fs.readJSON(DB_FILE, (err, db) => {
        if (err) {
          console.error(err)
          resolve(undefined);
        }
        if (db !== undefined && db.candidates !== undefined) {
          resolve(db.candidates[id]);
        } else {
          resolve(undefined);
        }
      })
    }, (reason) => {
      resolve(undefined);
    });
  }

  addLikeToCandidate(id) {
    return new Promise((resolve, reject) => {
      const x = this.state.addLikeToCandidate(id);
      fs.writeJSON(DB_FILE, this.state.getCandidates());
      resolve(x);
    });
  }

  setCandidateEnabledStatus(id, enabled = true) {
    return new Promise((resolve, reject) => {
      const x = this.state.setCandidateEnabledStatus(id, enabled);
      fs.writeJSON(DB_FILE, this.state.getCandidates());
      resolve(x);
    })
  }
}

module.exports = DatabaseHandler;